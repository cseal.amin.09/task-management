### Project installation

First git clone by this command:

```
git clone git@gitlab.com:cseal.amin.09/task-management.git
```  

Then Run These Command

```
1. cd task-management
2. git checkout develop
3. composer install
4. php artisan migrate (First Create .env file with bellow my example .env code, then run this command)

```

### NPM Run

```
1. npm install
2. npm run dev
```

### Queue Run

```
php artisan queue:work 
```

### Project Run

```
php artisan serve
```

### Live
```
http://127.0.0.1:8000
```

### Use This .env file

```
APP_NAME="Task Management System"
APP_ENV=local
APP_KEY=base64:8H/2+4bW4EDJ/Zxhp7tlD1ksVMABVn1CU7CL2tbiUL0=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=task_management
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=database
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=debd4741aadd74
MAIL_PASSWORD=6db12cc708bac4
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=cseal.amin.09@gmail.com
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_HOST=
PUSHER_PORT=443
PUSHER_SCHEME=https
PUSHER_APP_CLUSTER=mt1

VITE_APP_NAME="${APP_NAME}"
VITE_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
VITE_PUSHER_HOST="${PUSHER_HOST}"
VITE_PUSHER_PORT="${PUSHER_PORT}"
VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"
VITE_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```
